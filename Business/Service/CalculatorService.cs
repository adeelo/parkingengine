﻿using Business.Service.IService;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Service
{
    public class CalculatorService : ICalculatorService
    {
        
        public async Task<ParkingTotalDTO> Calculate(CarEntryExitDTO input)
        {
            DayOfWeek dayOfWeek = input.Entry.DayOfWeek;
            var numberOfDays = (input.Exit - input.Entry).TotalDays;
            var numberOfHours = (input.Exit - input.Entry).TotalHours;
             
            if (dayOfWeek != DayOfWeek.Saturday && dayOfWeek != DayOfWeek.Sunday)
            {


                var early = EarlyBird(input, numberOfDays, numberOfHours);
                if (early != null)
                    return early;

                var night = NightRate(input, numberOfDays, numberOfHours);
                if (night != null)
                    return night;

               return StandardRate(numberOfDays, numberOfHours, 5.00);

            }
            else
            {
                var weekend = WeekendRate(input, numberOfDays, numberOfHours);

                if (weekend != null)
                    return weekend;
            }
            return null ;
        }


        private ParkingTotalDTO EarlyBird(CarEntryExitDTO input, double numberOfDays, double numberOfHours)
        {
            var earlyBird = new EarlyBirdTimeSetting();
            if (input.Entry.TimeOfDay >= earlyBird.EarliestEntrytime && input.Entry.TimeOfDay <= earlyBird.LatestEntrytime)
            {
                //check between range
                if (input.Exit.TimeOfDay >= earlyBird.EarliestExittime
                && input.Exit.TimeOfDay <= earlyBird.LatestExittime && numberOfDays < 1)
                {
                    return new ParkingTotalDTO
                    {
                        RateName = "Early Bird",
                        RateCost = $"${earlyBird.RateCost:0.00}"
                    };
                }


                return StandardRate(numberOfDays, numberOfHours , earlyBird.RateCost);

            }
            return null;
        }

        private ParkingTotalDTO NightRate(CarEntryExitDTO input, double numberOfDays, double numberOfHours)
        {
            var nightRate = new NightRateSettings();

        

            if (input.Entry.TimeOfDay >= nightRate.EarliestEntrytime
               && (input.Entry.TimeOfDay <= nightRate.LatestEntrytime))
            {
                if (input.Exit.TimeOfDay <= nightRate.LatestExittime
                && numberOfDays < 1)
                {
                    
                    return new ParkingTotalDTO
                    {
                        RateName = "Night Rate",
                        RateCost = $"${nightRate.RateCost:0.00}"
                    };
                }
                //calculate standard rate
                return StandardRate(numberOfDays, numberOfHours, nightRate.RateCost);
            }

            return null;

        }

        private ParkingTotalDTO WeekendRate(CarEntryExitDTO input, double numberOfDays, double numberOfHours)
        {
            double weekendRate = 10.00;
            if (input.Exit.DayOfWeek == DayOfWeek.Saturday || input.Exit.DayOfWeek == DayOfWeek.Sunday )
            { 
                return new ParkingTotalDTO
                {
                    RateName = "Weekend Rate",
                    RateCost = "$10.00"
                };
            }

           return StandardRate(numberOfDays, numberOfHours, weekendRate);

        }

        private ParkingTotalDTO StandardRate(double numberOfDays, double numberOfHours, double rate)
        {
            double check = numberOfHours % 24;
            int totaldays = (int)Math.Abs(Math.Round(numberOfDays));
            
            //means weekend
            if (rate==10.00)
            {
                totaldays = totaldays / 2;
            }
            double max = 20, perhour = 5;
            string total = "";
            if (check > 3)
            {
                if (totaldays >= 1)
                {
                    total = ((max * totaldays) + rate).ToString("#.00");
                }
                else
                {
                    total = ((max * totaldays) + max).ToString("#.00");
                }

            }
            else if (check <= 3)
            {
                total = ((max * totaldays) + (Math.Ceiling(numberOfHours) * perhour)).ToString("#.00");
            }

            return new ParkingTotalDTO
            {
                RateName = "Standard Rate",
                RateCost = $"${total}"
            };
        }
    }
}
