﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Service.IService
{
    public interface ICalculatorService
    {
        Task<ParkingTotalDTO> Calculate(CarEntryExitDTO input);
    }
}
