using Business.Service;
using Models;
using System;
using System.Threading.Tasks;
using Xunit;

namespace ParkingRateEngine.UnitTest
{
    public class CarParkRateRepositoryTests
    { 
        [Fact]
        public void GivenTime_WhenEnterEarlyLeavebetweenRange_ReturnEarlyBirdRate()
        {

            //arrange
            var entry = new CarEntryExitDTO()
            {
                Entry = new DateTime(2021, 4, 2, 6, 0, 0),
                Exit = new DateTime(2021, 4, 2, 23, 30, 0)
            };

            var expected = new ParkingTotalDTO()
            {
                RateName = "Early Bird",
                RateCost = "$13.00"
            };

            var calc = new CalculatorService();
            //act        
            var result = calc.Calculate(entry);


            // assert  
            Assert.Equal(expected.RateCost, result.Result.RateCost);
            Assert.Equal(expected.RateName, result.Result.RateName);

        }

        [Fact]
        public void GivenTime_WhenEnterAfter6LeaveBetweenRange_ReturnNightRate()
        {

            //arrange
            var entry = new CarEntryExitDTO()
            {
                Entry = new DateTime(2021, 4, 2, 18, 0, 0),
                Exit = new DateTime(2021, 4, 3, 8, 0, 0)
            };

            var expected = new ParkingTotalDTO()
            {
                RateName = "Night Rate",
                RateCost = "$6.50"
            };

            var calc = new CalculatorService();
            //act        
            var result = calc.Calculate(entry);


            // assert  
            Assert.Equal(expected.RateCost, result.Result.RateCost);
            Assert.Equal(expected.RateName, result.Result.RateName);

        }




        [Fact]
        public void GivenTime_WhenEnterInWeekend_ReturnWeekendRate()
        {

            //arrange
            var entry = new CarEntryExitDTO()
            {
                Entry = new DateTime(2021, 4, 3, 0, 0, 0),
                Exit = new DateTime(2021, 4, 4, 23, 59, 0)
            };

            var expected = new ParkingTotalDTO()
            {
                RateName = "Weekend Rate",
                RateCost = "$10.00"
            };

            var calc = new CalculatorService();
            //act        
            var result = calc.Calculate(entry);


            // assert  
            Assert.Equal(expected.RateCost, result.Result.RateCost);
            Assert.Equal(expected.RateName, result.Result.RateName);

        }


        [Fact]
        public void GivenTime_WhenTime1Hour_StandardRate()
        {

            //arrange
            var entry = new CarEntryExitDTO()
            {
                Entry = new DateTime(2021, 4, 2, 11, 0, 0),
                Exit = new DateTime(2021, 4, 2, 12, 0, 0)
            };

            var expected = new ParkingTotalDTO()
            {
                RateName = "Standard Rate",
                RateCost = "$5.00"
            };

            var calc = new CalculatorService();
            //act        
            var result = calc.Calculate(entry);


            // assert  
            Assert.Equal(expected.RateCost, result.Result.RateCost);
            Assert.Equal(expected.RateName, result.Result.RateName);

        }


        [Fact]
        public void GivenTime_WhenTime1Hour30min_StandardRate()
        {

            //arrange
            var entry = new CarEntryExitDTO()
            {
                Entry = new DateTime(2021, 4, 2, 11, 0, 0),
                Exit = new DateTime(2021, 4, 2, 12, 30, 0)
            };

            var expected = new ParkingTotalDTO()
            {
                RateName = "Standard Rate",
                RateCost = "$10.00"
            };

            var calc = new CalculatorService();
            //act        
            var result = calc.Calculate(entry);


            // assert  
            Assert.Equal(expected.RateCost, result.Result.RateCost);
            Assert.Equal(expected.RateName, result.Result.RateName);

        }

        [Fact]
        public void GivenTime_WhenTimeNotInRangeOver3hours_StandardRate()
        {

            //arrange
            var entry = new CarEntryExitDTO()
            {
                Entry = new DateTime(2021, 4, 2, 11, 0, 0),
                Exit = new DateTime(2021, 4, 2, 14, 30, 0)
            };

            var expected = new ParkingTotalDTO()
            {
                RateName = "Standard Rate",
                RateCost = "$20.00"
            };

            var calc = new CalculatorService();
            //act        
            var result = calc.Calculate(entry);


            // assert  
            Assert.Equal(expected.RateCost, result.Result.RateCost);
            Assert.Equal(expected.RateName, result.Result.RateName);

        }
         


        [Fact]
        public void GivenTime_WhenEnterNotInRange_ReturnnormalRateFor3Hours()
        {

            //arrange
            var entry = new CarEntryExitDTO()
            {
                Entry = new DateTime(2021, 4, 2, 6, 0, 0),
                Exit = new DateTime(2021, 4, 2, 9 ,0, 0)
            };

            var expected = new ParkingTotalDTO()
            {
                RateName = "Standard Rate",
                RateCost = "$15.00"
            };

            var calc = new CalculatorService();
            //act        
            var result = calc.Calculate(entry);


            // assert  
            Assert.Equal(expected.RateCost, result.Result.RateCost);
            Assert.Equal(expected.RateName, result.Result.RateName);

        }


        [Fact]
        public void GivenTime_WhenEnterNotInRange_ReturnMaxRate()
        {

            //arrange
            var entry = new CarEntryExitDTO()
            {
                Entry = new DateTime(2021, 4, 2, 6, 0, 0),
                Exit = new DateTime(2021, 4, 2, 12, 0, 0)
            };

            var expected = new ParkingTotalDTO()
            {
                RateName = "Standard Rate",
                RateCost = "$20.00"
            };

            var calc = new CalculatorService();
            //act        
            var result = calc.Calculate(entry);


            // assert  
            Assert.Equal(expected.RateCost, result.Result.RateCost);
            Assert.Equal(expected.RateName, result.Result.RateName);

        }


        [Fact]
        public void GivenTime_WhenEnterNotInRangeAtNight_ReturnMaxRate()
        {

            //arrange
            var entry = new CarEntryExitDTO()
            {
                Entry = new DateTime(2021, 4, 2, 18, 0, 0),
                Exit = new DateTime(2021, 4, 2, 23, 0, 0)
            };

            var expected = new ParkingTotalDTO()
            {
                RateName = "Standard Rate",
                RateCost = "$20.00"
            };

            var calc = new CalculatorService();
            //act        
            var result = calc.Calculate(entry);


            // assert  
            Assert.Equal(expected.RateCost, result.Result.RateCost);
            Assert.Equal(expected.RateName, result.Result.RateName);

        }


        [Fact]
        public void GivenTime_WhenEnterNotInRangeAtNightNextDay_ReturnMaxRate()
        {

            //arrange
            var entry = new CarEntryExitDTO()
            {
                Entry = new DateTime(2021, 4, 2, 18, 0, 0),
                Exit = new DateTime(2021, 4, 3, 23, 59, 0)
            };

            var expected = new ParkingTotalDTO()
            {
                RateName = "Standard Rate",
                RateCost = "$26.50"
            };

            var calc = new CalculatorService();
            //act        
            var result = calc.Calculate(entry);


            // assert  
            Assert.Equal(expected.RateCost, result.Result.RateCost);
            Assert.Equal(expected.RateName, result.Result.RateName);

        }


        [Fact]
        public void GivenTime_EnterWeekendExitNextWeekDay_ReturnLowestRate()
        {

            //arrange
            var entry = new CarEntryExitDTO()
            {
                Entry = new DateTime(2021, 4, 3, 18, 0, 0),
                Exit = new DateTime(2021, 4, 5, 23, 59, 0)
            };

            var expected = new ParkingTotalDTO()
            {
                RateName = "Standard Rate",
                RateCost = "$30.00"
            };

            var calc = new CalculatorService();
            //act        
            var result = calc.Calculate(entry);


            // assert  
            Assert.Equal(expected.RateCost, result.Result.RateCost);
            Assert.Equal(expected.RateName, result.Result.RateName);

        }
    }
}
