﻿using Business.Service.IService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParkingRateEngine.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarRateController : ControllerBase
    {
        private readonly ICalculatorService _calculatorService;

        public CarRateController(ICalculatorService calculatorService)
        {
            _calculatorService = calculatorService;
        }

        [HttpPost]
        public async Task<ActionResult<ParkingTotalDTO>> CalculateRate([FromBody] CarEntryExitDTO carEntryExit)
        {
            try
            {
                if (carEntryExit.Entry >= carEntryExit.Exit)
                {
                   return BadRequest("StartDate is not before EndDate!");
                }

                var result = await _calculatorService.Calculate(carEntryExit);
                if (result==null)
                {
                    return BadRequest("Result Threw an error");

                }

                return result;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
           
             
        } 
    }
}
