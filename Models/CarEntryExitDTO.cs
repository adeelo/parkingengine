﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
   public class CarEntryExitDTO
    {
        [Required]
        public DateTime Entry { get; set; }

        [Required]
        public DateTime Exit { get; set; }
    }
}
