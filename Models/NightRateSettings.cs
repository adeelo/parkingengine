﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class NightRateSettings
    {
        public TimeSpan EarliestEntrytime { get; set; } = new TimeSpan(18, 0, 0);
        public TimeSpan LatestEntrytime { get; set; } = new TimeSpan(23, 59, 0);
        public TimeSpan LatestExittime { get; set; } = new TimeSpan(8, 0, 0);
        public double RateCost { get; set; } = 6.50;
    }
}
