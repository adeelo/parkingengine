﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class ParkingTotalDTO
    {
        public string RateName { get; set; }
        public string RateCost { get; set; }
    }
}
