﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class EarlyBirdTimeSetting
    {  
        public TimeSpan EarliestEntrytime { get; set; } = new TimeSpan(6, 0, 0);
        public TimeSpan LatestEntrytime { get; set; } = new TimeSpan(9, 0, 0);
        public TimeSpan EarliestExittime { get; set; } = new TimeSpan(15, 30, 0);
        public TimeSpan LatestExittime { get; set; } = new TimeSpan(23, 30, 0); 
        public double RateCost { get; set; } = 13.00;
    }
}
